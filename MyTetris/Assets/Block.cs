﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
        public float GetY()
    {
        return transform.position.y;
    }

    public float GetX()
    {
        return transform.position.x;
    }
}
