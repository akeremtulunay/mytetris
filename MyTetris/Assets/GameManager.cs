﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    TetronomeSpawner tetronomeSpawner;
    bool isGameOver = false;
    bool isDestroyingLines = false;
    int playerPoints = 0;
    private void Start()
    {
        tetronomeSpawner = FindObjectOfType<TetronomeSpawner>();
    }

    public void OnTetronomeDrop(Tetronome tetronome)
    {
        tetronome.CheckLines();
        if (tetronome.transform.position.y >= tetronomeSpawner.transform.position.y)
        {
            isGameOver = true;
            print("Game Over");
            return;
        }
        StartCoroutine(DestroyLines(tetronome));
        
        
    }

    private IEnumerator DestroyLines(Tetronome tetronome)
    {
        foreach (TetronomeBlock block in tetronome.GetComponentsInChildren<TetronomeBlock>())
        {
            if (block.willBeDestroyed)
            {
                if(block != null)
                {
                    isDestroyingLines = true;
                    float destroyedLineY = new float();
                    destroyedLineY = block.transform.position.y;
                    DestroyLine(destroyedLineY);
                    playerPoints += 1000;
                    yield return new WaitForSeconds(0.2f);
                    FallLinesAbove(destroyedLineY);
                }

            }
        }

        if (isDestroyingLines)
        {
            isDestroyingLines = false;
        }
        tetronomeSpawner.SpawnTetronome();
        yield break;

    }

    private void FallLinesAbove(float y)
    {
        foreach (TetronomeBlock block in FindObjectsOfType<TetronomeBlock>())
        {
            if (block.transform.position.y > y)
            {
                block.Fall(true);
            }
        }
    }

    private void DestroyLine(float y)
    {
        foreach (TetronomeBlock block in FindObjectsOfType<TetronomeBlock>())
        {
            if (TetronomeBlock.IsApproxEqual(block.transform.position.y, y))
            {
                Destroy(block.gameObject);
            }
        }
    }

    public int GetPlayerPoints()
    {
        return playerPoints;
    }

}
