﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetronome : MonoBehaviour
{
    public bool isActive = true;
    public bool canGoRight = true;
    public bool canGoLeft = true;
    public bool canRotate = true;
    public float fallCountdown = 1;
    public GameManager gameManager;
    Touch touch;
    Vector2 touchStartPosition;
    Vector2 touchStartWorldPos;
    Vector2 lastTouchPos;
    Vector2 lastTouchWorldPos;
    Vector2 touchEndPosition;
    bool hasMoved = false;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    public void CheckLines()
    {
        foreach (TetronomeBlock block in GetComponentsInChildren<TetronomeBlock>())
        {
            block.CheckLine();
        }
    }

    void Update()
    {
        if (isActive)
        {
            CheckInput();
            if (Input.touches.Length > 0)
            {
               CheckMobileInput();
            }
            //Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //transform.position = new Vector3(Mathf.FloorToInt(touchPos.x) + 0.5f, transform.position.y);
            FallWithTime();
        }
    }

    private void CheckInput()
    {
        if (Input.GetKeyDown(KeyCode.S) && CanGoDown())
        {
            MoveDown();
        }
        if (Input.GetKeyDown(KeyCode.A) && canGoLeft)
        {
            MoveLeft();
        }
        if (Input.GetKeyDown(KeyCode.D) && canGoRight)
        {
            MoveRight();
        }
        if (canRotate && (Input.GetKeyDown(KeyCode.RightShift)))
        {
            Rotate();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            FallInstantly();
            Deactivate();
        }
    }

    private void CheckMobileInput()
    {

        touch = Input.touches[0];
        if (touch.phase == TouchPhase.Began)
        {
            touchStartPosition = touch.position;
            lastTouchWorldPos = Camera.main.ScreenToWorldPoint(touchStartPosition);
        }
        else if (touch.phase == TouchPhase.Ended)
        {
            touchEndPosition = touch.position;
            //if (touchEndPosition.x - touchStartPosition.x > 300f )
            //{
            //    if (canGoRight)
            //        MoveRight();

            //}
            //else if (touchEndPosition.x - touchStartPosition.x < -300f)
            //{
            //    if (canGoLeft)
            //        MoveLeft();
            //}
            if (touchEndPosition.y - touchStartPosition.y < -300f)
            {
                FallInstantly();
            }
            else
            {
                if (!hasMoved&&canRotate)
                {
                    Rotate();
                }                   
            }
        }
        else
        {
            if(touch.position.y - touchStartPosition.y > -300f)
            {
                Vector2 touchWorldPos = Camera.main.ScreenToWorldPoint(touch.position);
                if ((touchWorldPos.x - lastTouchWorldPos.x < -1  ) && canGoLeft)
                {
                    MoveLeft();
                    //transform.position += new Vector3(Mathf.FloorToInt(touchWorldPos.x - lastTouchPos.x) +   0.5f, 0);
                    lastTouchWorldPos = touchWorldPos;
                    //Mathf.Clamp(Mathf.FloorToInt(touchPos.x), -5.5f, 4.5f)
                    hasMoved = true;
                }else if((touchWorldPos.x - lastTouchWorldPos.x > 1) && canGoRight){

                    MoveRight();
                    lastTouchWorldPos = touchWorldPos;
                    hasMoved = true;
                }

            }
            else
            {
                FallInstantly();
                hasMoved = true;
            }
           
        }


    }
    

    private void MoveDown()
    {
        transform.position -= new Vector3(0, 1, 0);
    }

    private void MoveLeft()
    {
        transform.position -= new Vector3(1, 0, 0);
    }

    private void MoveRight()
    {
        transform.position -= new Vector3(-1, 0, 0);
    }

    private void Rotate()
    {
        bool doesOverlap = false;

        transform.Rotate(new Vector3(0, 0,-90), Space.Self);

        foreach (TetronomeBlock block in GetComponentsInChildren<TetronomeBlock>())
        {
            if (block.DoesOverlap())
            {
                doesOverlap = true;
            }
        }
        if (doesOverlap)
        {
            this.transform.position += Vector3.right;
            foreach (TetronomeBlock block in GetComponentsInChildren<TetronomeBlock>())
            {
                if (block.DoesOverlap())
                {
                    doesOverlap = true;
                }
            }
        }

        if (doesOverlap)
        {
            doesOverlap = false;
            this.transform.position += Vector3.left*2;
            foreach (TetronomeBlock block in GetComponentsInChildren<TetronomeBlock>())
            {
                if (block.DoesOverlap())
                {
                    doesOverlap = true;
                }
            }
        }
        if (doesOverlap)
        {
            doesOverlap = false;
            this.transform.position += Vector3.right;
            foreach (TetronomeBlock block in GetComponentsInChildren<TetronomeBlock>())
            {
                if (block.DoesOverlap())
                {
                    doesOverlap = true;
                }
            }
            transform.Rotate(new Vector3(0, 0, 90), Space.Self);
        }                  
    }

    private void FallWithTime()
    {
        fallCountdown -= Time.deltaTime;
        if (fallCountdown <= 0)
        {
            if (CanGoDown())
            {
                MoveDown();
                fallCountdown = 1;
            }
        }
    }

    public void FallInstantly()
    {
        while (CanGoDown())
        {
            MoveDown();
        }
    }

    public void DeactivateSide(int i)
    {
        if (i == 0)
        {
            return;
        }

        if (i == 1)
        {
            canGoRight = false;
        }
        else if (i == 2){

        }
        else
        {
            canGoLeft = false;
        }
    }

    public void ActivateSide(int i)
    {
        if (i == 0)
        {
            return;
        }

        if (i == 1)
        {
            canGoRight = true;
        }
        else if (i == 2)
        {
        }
        else
        {
            canGoLeft = true;
        }
    }

    public void Deactivate()
    {
        isActive = false;
        gameManager.OnTetronomeDrop(this);
    }
    
    public bool CanGoDown()
    {

        foreach (TetronomeBlock block in GetComponentsInChildren<TetronomeBlock>())
        {
            block.CheckCanFall();
            if (block.parentCanFall == false)
            {
                return false;
            }
        }
        return true;
    }
}
