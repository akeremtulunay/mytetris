﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetronomeBlock : Block
{
    public Tetronome parent;
    public bool willBeDestroyed = false;
    public bool hasFallen = false;
    public bool parentCanFall = true;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (parent.isActive && !DoesOverlap())
        {
            Block collidedTransform = collision.GetComponent<Block>();
            TetronomeBlock CollidedTetronomeBlock = collision.gameObject.GetComponent<TetronomeBlock>();
            if (CollidedTetronomeBlock != null && CollidedTetronomeBlock.GetParent().Equals(parent))
            {                
                return;               
            }
            if (IsAtSameLine(collidedTransform))
            {
                parent.DeactivateSide(GetCollisionSideX(collidedTransform));
            }
            if (IsAtSameColumn(collidedTransform))
            {
                if (GetCollisionSideY(collidedTransform) < 0)
                {
                    parent.DeactivateSide(2);
                    //parent.Deactivate();
                    StartCoroutine(DeactivateTetronome());
                }
            }

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Block collidedTransform = collision.GetComponent<Block>();
        parent.ActivateSide(GetCollisionSideX(collidedTransform));
        if (CheckCanFall())
        {
            parent.ActivateSide(2);
        }
       
    }

    private IEnumerator DeactivateTetronome()
    {
        yield return new WaitForSeconds(0.9f);
        parent.FallInstantly();
        if (!CheckCanFall() && parent.isActive)
        {
            parent.Deactivate();
        }
        yield break;
    }

    public void CheckLine()
    {
        CheckRight(this);
    }

    public bool CheckRight(TetronomeBlock lastLeftChecked)
    {     
        Vector3 raycastOrigin = transform.position + Vector3.right;
        RaycastHit2D hit = Physics2D.Raycast(raycastOrigin, Vector2.right, 0.1f);
        int checkedBlockType = CheckHit(hit);
        if (checkedBlockType == 2)
        {
            if (lastLeftChecked != null)
            {
                willBeDestroyed = lastLeftChecked.CheckLeft(null);
                return willBeDestroyed;
            }
            else
            {
                willBeDestroyed = true;
                return true;
            }
        }else if(checkedBlockType == 1)
        {
            TetronomeBlock lastRightChecked = hit.collider.GetComponent<TetronomeBlock>();
            if (lastRightChecked.willBeDestroyed)
            {
                return true;
            }
            if (lastLeftChecked != null)
            {

                willBeDestroyed = lastLeftChecked.CheckLeft(lastRightChecked);
                return willBeDestroyed;
            }
            else
            {
                willBeDestroyed = lastRightChecked.CheckRight(null);
                return willBeDestroyed;
            }
        }
        else
        {
            return false;
        }       
    }

    public bool CheckLeft(TetronomeBlock lastRightChecked)
    {
        Vector3 raycastOrigin = transform.position + Vector3.left;
        RaycastHit2D hit = Physics2D.Raycast(raycastOrigin, Vector2.left, 0.1f);
        int checkedBlockType = CheckHit(hit);
        if (checkedBlockType == 2)
        {
            if (lastRightChecked != null)
            {
                willBeDestroyed = lastRightChecked.CheckRight(null);
                return willBeDestroyed;
            }
            else
            {
                willBeDestroyed = true;
                return true;
            }
        }
        else if (checkedBlockType == 1)
        {
            TetronomeBlock lastLeftChecked = hit.collider.GetComponent<TetronomeBlock>();
            if (lastRightChecked != null)
            {              
                willBeDestroyed = lastRightChecked.CheckRight(lastLeftChecked);
                return willBeDestroyed;
            }
            else
            {                
                willBeDestroyed = lastLeftChecked.CheckLeft(null);
                return willBeDestroyed;
            }
        }
        else
        {
            return false;
        }
    }

    public int CheckHit(RaycastHit2D hit)
    {
        if (hit.collider == null)
        {
            return 0;
        }
        else
        {
            if (hit.collider.GetComponent<TetronomeBlock>() != null)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }

    }

  

    public Tetronome GetParent()
    {
        return parent;
    }



    public static bool IsApproxEqual(float x, float y)
    {
        if (Mathf.Abs(x - y) <= 0.1f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool IsAtSameLine(Block block)
    {
        if (IsApproxEqual(block.GetY(), GetY()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool IsAtSameColumn(Block block)
    {
        if(IsApproxEqual(block.GetX(), GetX()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public int GetCollisionSideX(Block block)
    {
        float differenceX = block.GetX() - GetX();

        if (IsApproxEqual(differenceX , 0))
        {
            return 0;
        }

        if(differenceX > 0)
        {
            return 1;
        }
        else
        {

            return -1;
        }
    }

    public int GetCollisionSideY(Block block)
    {
        if (block.GetY() - GetY() > 0)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    public bool DoesOverlap()
    {
        foreach(Block block in FindObjectsOfType<Block>())
        {
            if (block.GetComponent<TetronomeBlock>() && block.GetComponent<TetronomeBlock>().parent.Equals(parent))
            {
                continue;
            }

            if(IsAtSameColumn(block) && IsAtSameLine(block))
            {
                return true;
            }
        }
        return false;
    }

    public void Fall(bool isForced)
    {

        if (isForced)
        {
            this.transform.position += Vector3.down;
        }
        else
        {
            if (CheckCanFall())
            {
                this.transform.position += Vector3.down;
            }
        }
        
    }

    public bool CheckCanFall()
    {
        Vector3 raycastOrigin = transform.position + Vector3.down;
        RaycastHit2D hit = Physics2D.Raycast(raycastOrigin, Vector2.down, 0.1f);
        int checkedBlockType = CheckHit(hit);
        if (checkedBlockType == 0)
        {
            parentCanFall = true;
            return true;
        }else if(checkedBlockType == 1)
        {
            if (hit.collider.gameObject.GetComponent<TetronomeBlock>().GetParent() != parent)
            {
                parentCanFall = false;
                return false;
            }
            else
            {
                parentCanFall = true;
                return false;
            }

        }
        else
        {
            parentCanFall = false;
            return false;
        }

    }
}
