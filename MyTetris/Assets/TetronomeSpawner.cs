﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetronomeSpawner : MonoBehaviour
{
    [SerializeField] Tetronome[] tetronomeToSpawn;
    bool isActive = true;
    Queue queue = new Queue();
    int nextTetronomeIndex;
    NextDisplay nextDisplay;
    // Start is called before the first frame update

    private void Start()
    {
        nextDisplay = FindObjectOfType<NextDisplay>();
        int tetronomeIndex = Random.Range(0, tetronomeToSpawn.Length);
        GameObject.Instantiate(tetronomeToSpawn[tetronomeIndex], transform.position, Quaternion.identity);
        for(int i = 0; i < 3; i++)
        {
            nextTetronomeIndex = DetermineNextTetronome();
            queue.Enqueue(nextTetronomeIndex);
            nextDisplay.nextDisplays[i].sprite = nextDisplay.tetronomeSprites[nextTetronomeIndex];
            nextDisplay.nextDisplays[i].SetNativeSize();
        }
 
    }

    public void SpawnTetronome()
    {
        if (isActive)
        {
            int tetronomeIndex = DetermineNextTetronome();
            GameObject.Instantiate(tetronomeToSpawn[(int)queue.Dequeue()], transform.position, Quaternion.identity);
            nextTetronomeIndex = DetermineNextTetronome();
            queue.Enqueue(nextTetronomeIndex);
            for (int i = 0; i < nextDisplay.nextDisplays.Length - 1; i++)
            {
                nextDisplay.nextDisplays[i].sprite = nextDisplay.nextDisplays[i + 1].sprite;
                nextDisplay.nextDisplays[i].SetNativeSize();
            }
            nextDisplay.nextDisplays[nextDisplay.nextDisplays.Length - 1].sprite = nextDisplay.tetronomeSprites[nextTetronomeIndex];
            nextDisplay.nextDisplays[nextDisplay.nextDisplays.Length - 1].SetNativeSize();


        }

    }

    public int DetermineNextTetronome()
    {
        return Random.Range(0, tetronomeToSpawn.Length);
    }

    public void Deactivate()
    {
        isActive = false;
    }
}
